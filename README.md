# TJV - semestral

## Brief description

In this semestral project I will be building client-server application that allows users to share and comment on different food recipes. All recipes will be separated into different categories based on country of origin.

## Running

Make sure that the docker daemon is running.

```bash
sudo systemctl start docker
```

Build and run docker.

```bash
sudo docker-compose build
sudo docker-compose up --remove-orphans
```

After this the application should accesible from web browser [link](http://localhost:8080/) and the documentation should be accesible [here](http://localhost:8080/swagger-ui/index.html).

## Additional source code

The frontend was written in React.js and the source code can be found [here](https://gitlab.fit.cvut.cz/stojkiva/tjv_front). It is a bit of a mess =(

## Client business operation

When user tries to add a recipe, client will send a request to server to perform different validations steps such as:

1. **File format** - only png and jpg formats will be allowed to be used as thumbnails for recipe
2. **Image dimensions** - only images with dimensions up to 2000x2000px will be allowed to be used as thumbnails.

After the previously mentioned steps the recipe will be added to the database.

This operation involves several steps and although it is viewed as a single operation from the client viewpoint, it consists of multiple steps in different parts of server side application. 

## Model description

![Diagram](diagram.png)

## Complex query

A complex query that will allow user to view all recipes from specified category on which he has left a comment

```sql
SELECT r.*
FROM Recipe AS r
INNER JOIN (
    SELECT rc.recipe_id
    FROM RecipeComment AS rc
    INNER JOIN User AS u ON rc.user_email = u.email
    INNER JOIN Category AS c ON r.category_id = c.category_id
    WHERE u.email = 'user_email' 
    AND c.name = 'category_id'
) AS subquery
ON r.recipe_id = subquery.recipe_id;

```

package com.semestar.stojka.semestral.repository;

import com.semestar.stojka.semestral.domain.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    public void testFindByNonExistentId() {
        Optional<User> foundUserOptional = userRepository.findById("nonexistent@example.com");

        assertTrue(foundUserOptional.isEmpty());
    }
}

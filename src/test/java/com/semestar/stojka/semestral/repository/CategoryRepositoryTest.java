package com.semestar.stojka.semestral.repository;

import com.semestar.stojka.semestral.domain.Category;
import com.semestar.stojka.semestral.repository.CategoryRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
public class CategoryRepositoryTest {

    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private CategoryRepository categoryRepository;

    @Test
    public void testFindByName() {
        Category category = new Category("TestCategory", null);
        testEntityManager.persist(category);

        Optional<Category> foundCategoryOptional = categoryRepository.findById("TestCategory");

        assertTrue(foundCategoryOptional.isPresent());
        Category foundCategory = foundCategoryOptional.get();
        assertEquals("TestCategory", foundCategory.getName());
    }

    @Test
    public void testFindByNonExistentName() {
        Optional<Category> foundCategoryOptional = categoryRepository.findById("NonExistentCategory");

        assertTrue(foundCategoryOptional.isEmpty());
    }
}

package com.semestar.stojka.semestral.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.semestar.stojka.semestral.SemestralApplication;
import com.semestar.stojka.semestral.api.controller.RecipeController;
import com.semestar.stojka.semestral.domain.Category;
import com.semestar.stojka.semestral.domain.Recipe;
import com.semestar.stojka.semestral.domain.User;
import com.semestar.stojka.semestral.service.CategoryService;
import com.semestar.stojka.semestral.service.RecipeService;
import com.semestar.stojka.semestral.service.UserService;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {SemestralApplication.class})
@WebMvcTest(RecipeController.class)
@AutoConfigureMockMvc(addFilters = false)
public class RecipeControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;
    @MockBean
    private RecipeService recipeService;
    @MockBean
    private CategoryService categoryService;

    @Autowired
    private ObjectMapper objectMapper;


    @Test
    void testGetRecipes() {
        Recipe r1 = new Recipe();
        r1.setId(0l);
        r1.setName("name");
        r1.setContent("test");

        Recipe r2 = new Recipe();
        r2.setId(1l);
        r2.setName("name2");
        r2.setContent("test2");

        var expected = new ArrayList<Recipe>();
        expected.add(r1);
        expected.add(r2);

        Mockito.when(recipeService.readAll()).thenReturn(expected);

        assertDoesNotThrow(() -> {
            mockMvc.perform(
                    MockMvcRequestBuilders
                            .get("/api/recipes")
            )
            .andExpect(status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
            .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value("name"))
            .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(0L))
            .andExpect(MockMvcResultMatchers.jsonPath("$[1].id").value(1L));
        });
    }

    @Test
    void testDeleteLike() throws Exception {
        Mockito.when(recipeService.deleteLike(1L, "userId123")).thenReturn(true);

        Map<String, Object> requestMap = new HashMap<>();
        requestMap.put("recipeId", 1);
        requestMap.put("userId", "userId123");

        mockMvc.perform(MockMvcRequestBuilders
                        .delete("/api/recipes/like")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(requestMap)))
                .andExpect(status().isOk());

    }
    @Test
    void testAddLikeNegative() {
        assertDoesNotThrow(() -> {
            mockMvc.perform(
                    MockMvcRequestBuilders
                            .post("/api/recipes/like")
            ).andExpect(status().isBadRequest());
        });

    }
}

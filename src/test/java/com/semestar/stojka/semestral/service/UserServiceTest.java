package com.semestar.stojka.semestral.service;

import com.semestar.stojka.semestral.domain.Recipe;
import com.semestar.stojka.semestral.domain.User;
import com.semestar.stojka.semestral.repository.RecipeRepository;
import com.semestar.stojka.semestral.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private RecipeRepository recipeRepository;

    @InjectMocks
    private UserServiceImpl userService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void findByString() {
        User user1 = new User();
        user1.setEmail("user1@example.com");
        user1.setName("User One");
        user1.setPassword("password");

        User user2 = new User();
        user2.setEmail("user2@example.com");
        user2.setName("User two");
        user2.setPassword("password");

        List<User> userList = Arrays.asList(user1, user2);
        when(userRepository.findAll()).thenReturn(userList);

        List<User> result = userService.findByString("user");
        assertEquals(2, result.size());
        assertTrue(result.contains(user1));
        assertTrue(result.contains(user2));
    }

    @Test
    void saveUser() {
        User existingUser = new User();
        existingUser.setEmail("existingUser@example.com");
        existingUser.setName("Existing User");
        when(userRepository.findById("existingUser@example.com")).thenReturn(Optional.of(existingUser));

        when(userRepository.save(any(User.class))).thenAnswer(invocation -> invocation.getArgument(0));

        User newUser = new User();
        newUser.setEmail("existingUser@example.com");
        newUser.setName("Updated User");
        assertTrue(userService.saveUser("existingUser@example.com", newUser));

        assertEquals("Updated User", existingUser.getName());
    }

    @Test
    void saveUserNonexistentUser() {
        when(userRepository.findById("nonexistentUser@example.com")).thenReturn(Optional.empty());

        NoSuchElementException exception = assertThrows(NoSuchElementException.class, () ->
                userService.saveUser("nonexistentUser@example.com", new User()));

        assertEquals("No such user", exception.getMessage());
    }

    @Test
    void create() throws Exception {
        when(userRepository.existsById("newUser@example.com")).thenReturn(false);

        when(userRepository.save(any(User.class))).thenAnswer(invocation -> invocation.getArgument(0));

        User newUser = new User();
        newUser.setEmail("email@example.com");
        newUser.setName("new user");
        newUser.setPassword("password");
        User createdUser = userService.create(newUser);

        assertEquals(newUser, createdUser);
    }

    @Test
    void deleteUserNonexistentUser() {
        when(userRepository.findById("nonexistentUser@example.com")).thenReturn(Optional.empty());

        NoSuchElementException exception = assertThrows(NoSuchElementException.class, () ->
                userService.deleteUser("nonexistentUser@example.com"));

        assertEquals("No such user", exception.getMessage());
    }
}


package com.semestar.stojka.semestral.service;

import com.semestar.stojka.semestral.domain.Category;
import com.semestar.stojka.semestral.domain.Comment;
import com.semestar.stojka.semestral.domain.Recipe;
import com.semestar.stojka.semestral.repository.CategoryRepository;
import com.semestar.stojka.semestral.repository.CommentRepository;
import com.semestar.stojka.semestral.repository.RecipeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class CategoryServiceTest {

    @Mock
    private CategoryRepository categoryRepository;

    @Mock
    private RecipeRepository recipeRepository;

    @Mock
    private CommentRepository commentRepository;

    @InjectMocks
    private CategoryServiceImpl categoryService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }


    @Test
    void updateCategoryNonexistentCategory() {
        when(categoryRepository.findById("NonexistentCategory")).thenReturn(Optional.empty());

        NoSuchElementException exception = assertThrows(NoSuchElementException.class, () ->
                categoryService.updateCategory("NonexistentCategory", "NewCategory"));

        assertEquals("Wrong category id", exception.getMessage());
    }

    @Test
    void findRecipe() {
        Category category1 = new Category("Category1", Collections.emptyList());
        Category category2 = new Category("Category2", Collections.emptyList());
        when(categoryRepository.findAll()).thenReturn(Arrays.asList(category1, category2));

        List<Category> result = categoryService.findRecipe("Category");
        assertEquals(2, result.size());
        assertTrue(result.contains(category1));
        assertTrue(result.contains(category2));
    }
}

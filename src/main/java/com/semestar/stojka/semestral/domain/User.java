package com.semestar.stojka.semestral.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.*;

@Entity
@Getter
@Setter
@Table(name="users")
@AllArgsConstructor
@NoArgsConstructor
public class User implements EntityWithId<String>{

    @Id
    private String email;
    @Column(name = "name")
    private String name;
    @Column(name = "password")
    private String password;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Comment> comments = new HashSet<>();

    @ManyToMany(mappedBy = "likedBy")
    @JsonIgnoreProperties("likedBy")
    private List<Recipe> likedRecipes = new ArrayList<>();

    @Override
    public String getId() {
        return email;
    }
}


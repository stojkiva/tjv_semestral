package com.semestar.stojka.semestral.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.*;

@Table(name="recipe")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class Recipe implements EntityWithId<Long>{
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;
    @Column(name = "thumbnail")
    private String thumbnail;
    @Override
    public Long getId() {
        return id;
    }

    @Column(name = "content")
    private String content;

    @ManyToOne
    @JsonIgnoreProperties("recipes")
    @JoinColumn(name = "category")
    private Category category;

    @ManyToMany
    @JoinTable(
            name = "recipe_user_likes",
            joinColumns = @JoinColumn(name = "recipe_id"),
            inverseJoinColumns = @JoinColumn(name = "user_email")
    )
    @JsonIgnoreProperties("likedRecipes")
    private List<User> likedBy = new ArrayList<>();

    @OneToMany(mappedBy = "recipe")
    private Set<Comment> comments = new HashSet<>();

}
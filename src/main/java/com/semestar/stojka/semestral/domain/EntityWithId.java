package com.semestar.stojka.semestral.domain;

public interface EntityWithId<ID> {
    ID getId();
}
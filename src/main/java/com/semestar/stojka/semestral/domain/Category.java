package com.semestar.stojka.semestral.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Table(name="categories")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class Category implements EntityWithId<String>{
    @Id
    @Column(name = "name")
    private String name;

    @JsonIgnoreProperties({"category", "likedBy", "comment"})
    @OneToMany(mappedBy = "category", fetch = FetchType.EAGER) // Refers to the "category" field in the Recipe entity
    private List<Recipe> recipes;

    @Override
    public String getId() {
        return name;
    }
}
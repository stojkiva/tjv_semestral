package com.semestar.stojka.semestral.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Table(name="comments")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class Comment implements EntityWithId<Long>{
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "text")
    private String text;

    @ManyToOne
    @JoinColumn(name = "recipe_id")
    @JsonIgnoreProperties({"comments", "likedBy"})
    private Recipe recipe;

    @ManyToOne
    @JoinColumn(name = "user_id")
    @JsonIgnoreProperties({"comments", "liked", "likedRecipes"})
    private User user;

    @Override
    public Long getId() {
        return id;
    }
}
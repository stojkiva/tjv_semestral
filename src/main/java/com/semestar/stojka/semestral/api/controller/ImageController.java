package com.semestar.stojka.semestral.api.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.*;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.InvalidPropertiesFormatException;
import java.util.UUID;

@RestController
@RequestMapping("/images")
@CrossOrigin(origins = "http://localhost:3000")
@Tag(name = "Images", description = "Endpoints for uploading and reading images")
public class ImageController {

    private static final String USER_HOME_DIRECTORY = System.getProperty("user.home") + "/images/";

    @Operation(
            summary = "Get image",
            description = "Returns an image based on filename"
    )
    @GetMapping("/{filename}")
    public ResponseEntity<byte[]> getImage(@PathVariable String filename) {
        try {
            Path imagePath = Paths.get(USER_HOME_DIRECTORY, filename);
            Resource resource = new UrlResource(imagePath.toUri());

            if (resource.exists() && resource.isReadable()) {
                byte[] data = Files.readAllBytes(imagePath);

                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.IMAGE_JPEG);
                return new ResponseEntity<>(data, headers, HttpStatus.OK);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Operation(
            summary = "Upload image",
            description = "Uploads image to the server and returns the filename"
    )
    @PostMapping(value = "upload")
    public ResponseEntity<String> uploadImage(@RequestParam("file") MultipartFile file) {
        if (file.isEmpty()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"result\": \"INVALID_REQUEST\"}");
        }

        try {
            BufferedImage image = ImageIO.read(file.getInputStream());
            if (image == null) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"result\": \"INVALID_REQUEST\"}");
            }

            int width = image.getWidth();
            int height = image.getHeight();
            if (width > 2000 || height > 2000) {
                throw new InvalidPropertiesFormatException("Invalid dimensions");
            }

            String uniqueFileName = UUID.randomUUID().toString() + "_" + file.getOriginalFilename();

            File directory = new File(USER_HOME_DIRECTORY);

            Path filePath = Paths.get(USER_HOME_DIRECTORY + uniqueFileName);
            Files.write(filePath, file.getBytes());

            return ResponseEntity.status(HttpStatus.OK).body("{\"result\": \"SUCCESS\", \"file\": \"" + uniqueFileName + "\"}");
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"result\": \"INVALID_REQUEST\"}");
        }
    }
}
package com.semestar.stojka.semestral.api.controller;

import com.semestar.stojka.semestral.domain.User;
import com.semestar.stojka.semestral.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping(value="/api/users")
@RequiredArgsConstructor
@Tag(name = "Users", description = "Endpoints for working with users")
public class UserController {

    private final UserService userService;

    @Operation(
            summary = "Get users",
            description = "Endpoint to get all users"
    )
    @GetMapping()
    public ResponseEntity<Iterable<User>> getAllUsers(){
        return ResponseEntity.ok(userService.readAll());
    }

    @Operation(
            summary = "Search",
            description = "Endpoint to search for user based on email or name"
    )
    @GetMapping(value = "/{search}")
    public ResponseEntity<Collection<User>> searchUsers(@PathVariable String search){
        return ResponseEntity.ok(userService.findByString(search));
    }

    @Operation(
            summary = "Submit user and recipe IDs",
            description = "Endpoint to submit user and recipe IDs"
    )
    @DeleteMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> deleteUser(@RequestBody Map<String, Object> request){
        try{
            String email = (String) request.get("email");
            userService.deleteUser(email);
            return ResponseEntity.ok("{\"result\": \"SUCCESS\"}");
        } catch (Exception err){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"result\": \"INVALID_REQUEST\"}");
        }
    }
    @Operation(
            summary = "Update",
            description = "Updates user information"
    )
    @PutMapping(value = "")
    public ResponseEntity<String> updateUsers(@RequestBody User user){
        try{
            userService.saveUser(user.getId(), user);
            return ResponseEntity.ok("{\"result\": \"SUCCESS\"}");

        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"result\": \"INVALID_REQUEST\"}");
        }
    }
    @Operation(
            summary = "New user",
            description = "Endpoint to add a new user"
    )
    @PostMapping()
    public ResponseEntity<String> addUser(@RequestBody User user){
        try{
            userService.create(user);

            return ResponseEntity.ok("{\"result\": \"SUCCESS\"}");
        }catch (Exception err){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"result\": \"INVALID_REQUEST\"}");
        }
    }
}
package com.semestar.stojka.semestral.api.controller;

import com.semestar.stojka.semestral.domain.Category;
import com.semestar.stojka.semestral.domain.Recipe;
import com.semestar.stojka.semestral.domain.User;
import com.semestar.stojka.semestral.service.CategoryService;
import com.semestar.stojka.semestral.service.RecipeService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping(value="/api/categories")
@RequiredArgsConstructor
@Tag(name = "Categories", description = "Endpoints for working with categories")
public class CategoryController {
    private final CategoryService categoryService;

    @Operation(
            summary = "Get all categories",
            description = "Endpoint that returns all categories"
    )
    @GetMapping()
    public ResponseEntity<Iterable<Category>> getAllRecipes(){
        return ResponseEntity.ok(categoryService.readAll());
    }

    @Operation(
            summary = "Search category",
            description = "Searches category based on name"
    )
    @GetMapping(value="/{search}")
    public ResponseEntity<Iterable<Category>> searchRecipes(@PathVariable String search){
        return ResponseEntity.ok(categoryService.findRecipe(search));
    }

    @Operation(
            summary = "Delete category",
            description = "Deletes category based on name"
    )
    @DeleteMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> deleteCategory(@RequestBody Map<String, Object> request) {
        try {
            String id = String.valueOf(request.get("name"));
            System.out.println(id);

            categoryService.deleteCategory(id);

            return ResponseEntity.status(HttpStatus.OK).body("{\"result\": \"SUCCESS\"}");
        } catch (Exception err) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"result\": \"INVALID_REQUEST\"}");
        }
    }

    @Operation(
            summary = "Add category",
            description = "Adds new category"
    )
    @PostMapping()
    public ResponseEntity<String> addCategory(@RequestBody Category category){
        try{
            categoryService.create(category);

            return ResponseEntity.status(HttpStatus.OK).body("{\"result\": \"SUCCESS\"}");
        }catch (Exception err){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"result\": \"INVALID_REQUEST\"}");
        }
    }

    @PutMapping()
    public ResponseEntity<String> updateCategory(@RequestBody Map<String, Object> request){
        try{
            String oldName = String.valueOf(request.get("oldName"));
            String name = String.valueOf(request.get("name"));
            categoryService.updateCategory(oldName, name);

            return ResponseEntity.status(HttpStatus.OK).body("{\"result\": \"SUCCESS\"}");
        }catch (Exception err){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"result\": \"INVALID_REQUEST\"}");
        }
    }
}
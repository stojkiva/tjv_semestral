package com.semestar.stojka.semestral.api.controller;

import com.semestar.stojka.semestral.domain.Comment;
import com.semestar.stojka.semestral.domain.Recipe;
import com.semestar.stojka.semestral.domain.User;
import com.semestar.stojka.semestral.service.RecipeService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping(value="/api/recipes")
@RequiredArgsConstructor
@Tag(name = "Recipes", description = "Endpoints for working with recipes")
public class RecipeController {
    private final RecipeService recipeService;

    @Operation(
            summary = "Read all",
            description = "Returns all recipes"
    )
    @GetMapping()
    public ResponseEntity<Iterable<Recipe>> getAllRecipes(){
        return ResponseEntity.ok(recipeService.readAll());
    }

    @Operation(
            summary = "Search recipe",
            description = "Searches recipes based on name"
    )
    @GetMapping(value="/{search}")
    public ResponseEntity<Iterable<Recipe>> searchRecipes(@PathVariable String search){
        return ResponseEntity.ok(recipeService.findRecipe(search));
    }

    @Operation(
            summary = "Delete recipe",
            description = "Deletes recipes and all dependant entities (likes, comments)"
    )
    @DeleteMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> deleteRecipe(@RequestBody Map<String, Object> request){
        try{
            Long id = Long.valueOf((Integer) request.get("id"));

            recipeService.deleteRecipe(id);

            return ResponseEntity.status(HttpStatus.OK).body("{\"result\": \"SUCCESS\"}");
        }catch (Exception err){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"result\": \"INVALID_REQUEST\"}");
        }
    }

    @Operation(
            summary = "Update",
            description = "Updates recipe based on id"
    )
    @PutMapping(value = "")
    public ResponseEntity<String> updateRecipe(@RequestBody Map<String, Object> request){
        try{
            String name = String.valueOf(request.get("name"));
            String content = String.valueOf(request.get("content"));
            Long id = Long.valueOf((Integer) request.get("id"));
            recipeService.update(id, name, content);
            return ResponseEntity.ok("{\"result\": \"SUCCESS\"}");
        }catch (Exception e) {
            return ResponseEntity.ok("{\"result\": \"INVALID_REQUEST\"}");
        }
    }

    @Operation(
            summary = "Add recipe",
            description = "Endpoint to create a new recipe"
    )
    @PostMapping()
    public ResponseEntity<String> addRecipe(@RequestBody Map<String, Object> request){
        try{
            String name = String.valueOf(request.get("name"));
            String text = String.valueOf(request.get("text"));
            String thumbnail = String.valueOf(request.get("thumbnail"));
            String category = String.valueOf(request.get("category"));

            recipeService.saveRecipe(name, text, thumbnail, category);

            return ResponseEntity.status(HttpStatus.OK).body("{\"result\": \"SUCCESS\"}");

        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"result\": \"INVALID_REQUEST\"}");
        }
    }

    @Operation(
            summary = "Like",
            description = "Endpoint to like a recipe"
    )
    @PostMapping(value = "/like")
    public ResponseEntity<String> addLike(@RequestBody Map<String, Object> request){
        try{
            Long id = Long.valueOf((Integer) request.get("recipeId"));
            String userId = String.valueOf(request.get("userId"));
            recipeService.addLike(id, userId);
            return ResponseEntity.status(HttpStatus.OK).body("{\"result\": \"SUCCESS\"}");
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"result\": \"INVALID_REQUEST\"}");
        }
    }

    @Operation(
            summary = "Delete like",
            description = "Endpoint to remove recipe from liked"
    )
    @DeleteMapping(value="/like")
    public ResponseEntity<String> deleteLike(@RequestBody Map<String, Object> request){
        try{
            Long id = Long.valueOf((Integer) request.get("recipeId"));
            String userId = String.valueOf(request.get("userId"));
            recipeService.deleteLike(id, userId);
            return ResponseEntity.status(HttpStatus.OK).body("{\"result\": \"SUCCESS\"}");
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"result\": \"INVALID_REQUEST\"}");
        }
    }

    @Operation(
            summary = "Add comment",
            description = "Endpoint to add a comment"
    )
    @PostMapping(value="/comment")
    public ResponseEntity<String> addComment(@RequestBody Map<String, Object> request){
        try{
            Long id = Long.valueOf((Integer) request.get("recipeId"));
            String userId = String.valueOf(request.get("userId"));
            String text = String.valueOf(request.get("text"));
            recipeService.addComment(id, userId, text);
            return ResponseEntity.status(HttpStatus.OK).body("{\"result\": \"SUCCESS\"}");
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"result\": \"INVALID_REQUEST\"}");
        }
    }
    @Operation(
            summary = "Delete comment",
            description = "Endpoint to delete a comment"
    )
    @DeleteMapping(value="/comment")
    public ResponseEntity<String> deleteComment(@RequestBody Map<String, Object> request){
        try{
            Long id = Long.valueOf((Integer) request.get("commentId"));
            recipeService.deleteComment(id);
            return ResponseEntity.status(HttpStatus.OK).body("{\"result\": \"SUCCESS\"}");
        }catch (Exception e){
            System.out.println(e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"result\": \"INVALID_REQUEST\"}");
        }
    }

    @Operation(
            summary = "Complex query",
            description = "Endpoint to perform a complex query. Return all recipes which have a comment by specified user and are a part of specified category"
    )
    @PostMapping(value = "/complex")
    public ResponseEntity<List<Recipe>> complexQuery(@RequestBody Map<String, Object> request){
        try {
            String email = String.valueOf(request.get("email"));
            String category = String.valueOf(request.get("category"));

            System.out.println(email);
            System.out.println(category);

            List<Recipe> recipes = recipeService.getRecipesByUserAndCategory(email, category);

            return ResponseEntity.ok(recipes);
        } catch (Exception err) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }
}
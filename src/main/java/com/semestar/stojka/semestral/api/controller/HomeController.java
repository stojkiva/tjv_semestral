package com.semestar.stojka.semestral.api.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

    @GetMapping("/")
    public String index() {
        return "index.html"; // Returns the name of your HTML file (e.g., index.html)
    }
}

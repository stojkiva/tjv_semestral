package com.semestar.stojka.semestral.repository;

import com.semestar.stojka.semestral.domain.Comment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommentRepository extends JpaRepository<Comment, Long> {
}
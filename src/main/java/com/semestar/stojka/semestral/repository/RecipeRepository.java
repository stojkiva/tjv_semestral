package com.semestar.stojka.semestral.repository;

import com.semestar.stojka.semestral.domain.Recipe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RecipeRepository extends JpaRepository<Recipe, Long> {
    @Query(value = "SELECT DISTINCT r " +
            "FROM Recipe r " +
            "JOIN r.comments c " +
            "JOIN c.user u " +
            "JOIN r.category cat " +
            "WHERE u.email = :userEmail " +
            "AND cat.name = :categoryName")
    List<Recipe> findRecipesByUserAndCategory(String userEmail, String categoryName);
}
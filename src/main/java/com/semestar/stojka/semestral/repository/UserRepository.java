package com.semestar.stojka.semestral.repository;

import com.semestar.stojka.semestral.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, String> {
}

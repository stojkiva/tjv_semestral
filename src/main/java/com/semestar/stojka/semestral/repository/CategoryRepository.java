package com.semestar.stojka.semestral.repository;

import com.semestar.stojka.semestral.domain.Category;
import com.semestar.stojka.semestral.domain.Recipe;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category, String> {

}

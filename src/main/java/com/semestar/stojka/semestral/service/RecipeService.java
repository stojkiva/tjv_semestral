package com.semestar.stojka.semestral.service;


import com.semestar.stojka.semestral.domain.Recipe;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RecipeService extends CrudService<Recipe, Long>{
    List<Recipe> findRecipe(String search);

    Recipe addComment(Long recipeId, String userId, String text);

    boolean deleteRecipe(Long recipeId);
    boolean deleteComment(Long commentId);

    boolean saveRecipe(String name, String text, String thumbnail, String category);

    boolean update(Long id, String name, String content);

    boolean addLike(Long recipeId, String email);

    boolean deleteLike(Long recipeId, String email);

    List<Recipe> getRecipesByUserAndCategory(String userEmail, String categoryName);
}
package com.semestar.stojka.semestral.service;

import com.semestar.stojka.semestral.domain.Category;
import com.semestar.stojka.semestral.domain.Comment;
import com.semestar.stojka.semestral.domain.Recipe;
import com.semestar.stojka.semestral.domain.User;
import com.semestar.stojka.semestral.repository.CategoryRepository;
import com.semestar.stojka.semestral.repository.CommentRepository;
import com.semestar.stojka.semestral.repository.RecipeRepository;
import com.semestar.stojka.semestral.repository.UserRepository;
import jakarta.annotation.PostConstruct;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class RecipeServiceImpl extends CrudServiceImpl<RecipeRepository, Recipe, Long> implements RecipeService {
    private final RecipeRepository recipeRepository;
    private final CategoryRepository categoryRepository;
    private final UserRepository userRepository;
    private final CommentRepository commentRepository;

    @Override
    public List<Recipe> findRecipe(String search) {
        return recipeRepository.findAll().stream().filter(in -> in.getName().toLowerCase().contains(search.toLowerCase())).toList();
    }
    public List<Recipe> getRecipesByUserAndCategory(String userEmail, String categoryName) {
        return recipeRepository.findRecipesByUserAndCategory(userEmail, categoryName);
    }
    public Recipe addRecipe(Recipe recipe, String categoryName){
        Optional<Category> optCategory = categoryRepository.findById(categoryName);

        if(optCategory.isEmpty())
            throw new NoSuchElementException("invalid Category Name");
        Category category = optCategory.get();

        recipe.setCategory(category);
        recipe.getCategory().getRecipes().add(recipeRepository.save(recipe));

        categoryRepository.save(category);
        return recipeRepository.save(recipe);
    }
    @Override
    public boolean addLike(Long recipeId, String email){
        Optional<User> optUser = userRepository.findById(email);
        if(optUser.isEmpty())
            throw new NoSuchElementException(("invalid user email"));
        User user = optUser.get();

        Optional<Recipe> optRecite = recipeRepository.findById(recipeId);
        if(optRecite.isEmpty())
            throw new NoSuchElementException("Invalid recipe id");

        Recipe recipe = optRecite.get();

        if(recipe.getLikedBy().contains(user)){
            throw new NoSuchElementException("Already liked");
        }
        recipe.getLikedBy().add(user);
        recipeRepository.save(recipe);

        user.getLikedRecipes().add(recipe);
        userRepository.save(user);

        return true;
    }
    @Override
    public boolean deleteLike(Long recipeId, String email){
        Optional<User> optUser = userRepository.findById(email);
        if(optUser.isEmpty())
            throw new NoSuchElementException(("invalid user email"));
        User user = optUser.get();

        Optional<Recipe> optRecite = recipeRepository.findById(recipeId);
        if(optRecite.isEmpty())
            throw new NoSuchElementException("Invalid recipe id");

        Recipe recipe = optRecite.get();

        recipe.getLikedBy().remove(user);
        recipeRepository.save(recipe);

        user.getLikedRecipes().remove(recipe);
        userRepository.save(user);

        return true;
    }
    @Override
    public boolean update(Long recipeId, String name, String content){
        Optional<Recipe> optionalRecipe = recipeRepository.findById(recipeId);

        if(optionalRecipe.isEmpty())
            throw new NoSuchElementException();

        Recipe rec = optionalRecipe.get();

        rec.setContent(content);
        rec.setName(name);

        recipeRepository.save(rec);

        return true;
    }

    public boolean deleteRecipe(Long recipeId){
        Optional<Recipe> optRecipe = recipeRepository.findById(recipeId);
        if(optRecipe.isEmpty())
            throw new NoSuchElementException("Invalid recipe id");

        Recipe recipe = optRecipe.get();

        Set<Comment> comments = recipe.getComments();
        for (Comment comment:
             comments) {
            commentRepository.deleteById(comment.getId());
        }

        recipeRepository.deleteById(recipeId);
        return true;
    }
    public boolean deleteComment(Long commentId){
        Optional<Comment> optComment = commentRepository.findById(commentId);

        if(optComment.isEmpty())
            throw new NoSuchElementException("Invalid comment id");

        Comment comment = optComment.get();
        comment.getUser().getComments().remove(comment);
        comment.getRecipe().getComments().remove(comment);
        commentRepository.deleteById(commentId);

        return true;
    }

    public boolean saveRecipe(String name, String text, String thumbnail, String categoryId){
        Recipe recipe = new Recipe();
        recipe.setName(name);
        recipe.setContent(text);
        recipe.setThumbnail(thumbnail);

        System.out.println(categoryId);
        Optional<Category> optionalCategory = categoryRepository.findById(categoryId);
        if(optionalCategory.isEmpty())
            throw new NoSuchElementException();
        System.out.println(categoryId);
        Category category = optionalCategory.get();
        recipe.setCategory(category);

        category.getRecipes().add(recipeRepository.save(recipe));

        categoryRepository.save(category);

        return true;
    }
    public Recipe addComment(Long recipeId, String userId, String text){
        Optional<User> optUser = userRepository.findById(userId);
        if(optUser.isEmpty())
            throw new NoSuchElementException(("invalid user email"));
        User user = optUser.get();

        Optional<Recipe> optRecite = recipeRepository.findById(recipeId);
        if(optRecite.isEmpty())
            throw new NoSuchElementException("Invalid recipe id");

        Recipe recipe = optRecite.get();

        Comment comment = new Comment();
        comment.setUser(user);
        comment.setRecipe(recipe);
        comment.setText(text);
        commentRepository.save(comment);

        user.getComments().add(comment);
        recipe.getComments().add(comment);

        userRepository.save(user);
        return recipeRepository.save(recipe);
    }

    public RecipeServiceImpl(RecipeRepository recipeRepository, CategoryRepository categoryRepository, UserRepository userRepository, CommentRepository commentRepository) {
        this.recipeRepository = recipeRepository;
        this.categoryRepository = categoryRepository;
        this.userRepository = userRepository;
        this.commentRepository = commentRepository;
    }

    @Override
    protected RecipeRepository getRepository() {
        return recipeRepository;
    }
}
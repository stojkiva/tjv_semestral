package com.semestar.stojka.semestral.service;

import com.semestar.stojka.semestral.domain.Category;

import java.util.List;

public interface CategoryService extends CrudService<Category, String>{
    List<Category> findRecipe(String search);
    boolean deleteCategory(String id);

    boolean updateCategory(String oldName, String name);
}
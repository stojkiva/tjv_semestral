package com.semestar.stojka.semestral.service;

import com.semestar.stojka.semestral.domain.User;

import java.util.List;

public interface UserService extends CrudService<User, String> {
    List<User> findByString(String search);
    @Override
    public User create(User entity) throws Exception;

    void deleteUser(String userId);

    boolean saveUser(String userId, User user);
}

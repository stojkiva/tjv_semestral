package com.semestar.stojka.semestral.service;

import com.semestar.stojka.semestral.domain.EntityWithId;

import java.util.Optional;

public interface CrudService<T extends EntityWithId<ID>, ID> {
    T create(T entity) throws Exception;

    Optional<T> readById(ID id);

    Iterable<T> readAll();

    T update(ID id, T entity);

    void deleteById(ID id);
}


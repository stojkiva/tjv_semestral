package com.semestar.stojka.semestral.service;

import com.semestar.stojka.semestral.domain.Category;
import com.semestar.stojka.semestral.domain.Comment;
import com.semestar.stojka.semestral.domain.Recipe;
import com.semestar.stojka.semestral.domain.User;
import com.semestar.stojka.semestral.repository.CategoryRepository;
import com.semestar.stojka.semestral.repository.CommentRepository;
import com.semestar.stojka.semestral.repository.RecipeRepository;
import com.semestar.stojka.semestral.repository.UserRepository;
import jakarta.annotation.PostConstruct;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;

@Service
public class CategoryServiceImpl extends CrudServiceImpl<CategoryRepository, Category, String> implements CategoryService {
    private final CategoryRepository categoryRepository;
    private final RecipeRepository recipeRepository;
    private final CommentRepository commentRepository;

    @Override
    public boolean updateCategory(String oldName, String name){
        Optional<Category> optionalCategory = categoryRepository.findById(oldName);
        if(optionalCategory.isEmpty())
            throw new NoSuchElementException("Wrong category id");
        Category category = optionalCategory.get();

//        Optional<Category> optionalCategory1 = categoryRepository.findById(name);
//        if(optionalCategory1.isPresent())
//            throw new NoSuchElementException("This category already exists");
        category.setName(name);
        System.out.println(name);

        categoryRepository.save(category);
        return true;
    }
    @Override
    public boolean deleteCategory(String id){
        Optional<Category> categoryOptional = categoryRepository.findById(id);
        if(categoryOptional.isEmpty())
            throw new NoSuchElementException();

        Category category = categoryOptional.get();
        for (Recipe recipe:
             category.getRecipes()) {

            for (Comment comment:
                    recipe.getComments()) {
                commentRepository.deleteById(comment.getId());
            }


            recipeRepository.deleteById(recipe.getId());
        }
        categoryRepository.delete(category);
        return true;
    }
    @Override
    public List<Category> findRecipe(String search) {
        return categoryRepository.findAll().stream().filter(in -> in.getName().toLowerCase().contains(search.toLowerCase())).toList();
    }

    public CategoryServiceImpl(CategoryRepository categoryRepository, RecipeRepository recipeRepository, CommentRepository commentRepository, UserRepository userRepository) {
        this.categoryRepository = categoryRepository;
        this.recipeRepository = recipeRepository;
        this.commentRepository = commentRepository;
    }

    @Override
    protected CategoryRepository getRepository() {
        return categoryRepository;
    }
}

package com.semestar.stojka.semestral.service;

import com.semestar.stojka.semestral.domain.Recipe;
import com.semestar.stojka.semestral.domain.User;
import com.semestar.stojka.semestral.repository.RecipeRepository;
import com.semestar.stojka.semestral.repository.UserRepository;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.util.Collection;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class UserServiceImpl extends CrudServiceImpl<UserRepository, User, String> implements UserService {
    private final UserRepository userRepository;
    private final RecipeRepository recipeRepository;

    @Override
    public List<User> findByString(String search) {
        return userRepository.findAll().stream()
                .filter(in ->
                        in.getEmail().toLowerCase().contains(search.toLowerCase()) || in.getName().toLowerCase().contains(search.toLowerCase()))
                .toList();
    }

    @Override
    public boolean saveUser(String userId, User userNew){
        Optional<User> userOptional = userRepository.findById(userNew.getId());

        if(userOptional.isEmpty())
            throw new NoSuchElementException("No such user");

        User user = userOptional.get();

        user.setName(userNew.getName());

        userRepository.save(user);

        return true;
    }
    @Override
    public User create(User entity) throws Exception {
        int strength = 10;
        BCryptPasswordEncoder bCryptPasswordEncoder =
                new BCryptPasswordEncoder(strength, new SecureRandom());
        entity.setPassword(bCryptPasswordEncoder.encode(entity.getPassword()));

        if (getRepository().existsById(entity.getId()))
            throw new Exception("Already Exists");

        return getRepository().save(entity);
    }
    @Override
    public void deleteUser(String userId){
        Optional<User> userOptional = userRepository.findById(userId);

        if(userOptional.isEmpty())
            throw new NoSuchElementException("No such user");

        User user = userOptional.get();

        for (Recipe recipe :
                user.getLikedRecipes()) {
            recipe.getLikedBy().remove(user);
            recipeRepository.save(recipe);
        }

        user.setLikedRecipes(null);
        userRepository.delete(user);
    }
    public UserServiceImpl(UserRepository userRepository, RecipeRepository recipeRepository) {
        this.userRepository = userRepository;
        this.recipeRepository = recipeRepository;
    }

    @Override
    protected UserRepository getRepository() {
        return userRepository;
    }

}

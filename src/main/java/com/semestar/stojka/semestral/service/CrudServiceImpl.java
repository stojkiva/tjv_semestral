package com.semestar.stojka.semestral.service;

import com.semestar.stojka.semestral.domain.EntityWithId;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public abstract class CrudServiceImpl<R extends CrudRepository<T, ID>, T extends EntityWithId<ID>, ID> implements CrudService<T, ID> {
    protected abstract R getRepository();

    @Override
    public T create(T entity) throws Exception {
        if (getRepository().existsById(entity.getId()))
            throw new Exception("Already Exists");

        return getRepository().save(entity);
    }

    @Override
    public Optional<T> readById(ID id) {
        return getRepository().findById(id);
    }

    @Override
    public Iterable<T> readAll() {
        return getRepository().findAll();
    }

    @Override
    public T update(ID id, T entity) {
        return getRepository().save(entity);
    }


    @Override
    public void deleteById(ID id) {
//        try {
        if (!getRepository().existsById(id))
            throw new IllegalArgumentException("invalid id");
//        } catch (IllegalArgumentException e) {
//            //recovery
//        }

        getRepository().deleteById(id);
    }
}
